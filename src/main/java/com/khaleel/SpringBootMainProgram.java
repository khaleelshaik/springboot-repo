package com.khaleel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMainProgram {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMainProgram.class, args);
	}

}
