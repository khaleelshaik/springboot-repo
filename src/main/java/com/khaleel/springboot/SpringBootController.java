package com.khaleel.springboot;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringBootController 
{
	@Value("${test}")
	private String value;
	
	@GetMapping(path="/")
	public String getString() {
		InetAddress ip;
        String hostname;
        try {
            ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
            return "Welcome " + value + ", " + "Current IP Address : {" + ip + "}, Current HostName : {" + hostname + "}";
 
        } catch (UnknownHostException e) {
        	return "Exception Occurred : " + e.getMessage(); 
        }
	}

	@GetMapping(path="/{getParam}")
	public String getStringPathVariable(@PathVariable String getParam) {
		InetAddress ip;
        String hostname;
        try {
            ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
            return "Welcome " + getParam + ", " + "Current IP Address : {" + ip + "}, Current HostName : {" + hostname + "}";
 
        } catch (UnknownHostException e) {
        	return "Exception Occurred : " + e.getMessage(); 
        }
	}

	@GetMapping(path="/Get/")
	public String getStringValue() {
		return "khaleel";
	}
}
