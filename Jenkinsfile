pipeline {
  environment {
    registry = "khaleeldocker/springboot-test"
    registryCredential = 'docker-id'
    dockerImage = ''
  }
  agent any
  stages {
    stage('Cloning Git') {
      steps {
        git 'https://gitlab.com/khaleelshaik/springboot-repo.git'
      }
    }
    stage ('Build Project') {
        steps {
            sh 'mvn -Dmaven.test.failure.ignore=true clean install' 
        }
    }
    stage ('SonarQube Analysis') {
        steps {
            withSonarQubeEnv('Local Sonar') {
                sh 'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar'
            }
        }
    }
    stage ('SonarQube Quality Gate') {
        steps {
            script {
                sleep 10
                timeout(time: 1, unit: 'HOURS') { // Just in case something goes wrong, pipeline will be killed after a timeout
                    def qg = waitForQualityGate() // Reuse taskId previously collected by withSonarQubeEnv
                    if (qg.status != 'OK') {
                        error "Pipeline aborted due to quality gate failure: ${qg.status}"
                    }
                }
            }
        }
    }
    stage('Building image') {
      steps{
        script {
          dockerImage = docker.build(registry + ":$BUILD_NUMBER")
        }
      }
    }
    stage('Deploy Image') {
      steps{
        script {
          docker.withRegistry( '', registryCredential ) {
            dockerImage.push()
          }
        }
      }
    }
    stage('Remove Unused docker image') {
      steps{
        sh "docker rmi $registry:$BUILD_NUMBER"
      }
    }
  }
}